## =============================================================================
## Project 2
## Problem 5-d-FIRST PART
##
## LINEAR PERCEPTRON
##
## Train the linear perceptron, with the number of epochs set to EPOCHS = 1, 2,
## ..., EPOCHS_MAX. After training the linear perceptron, normalized the learned
## weight vector. Then, based on the results of this program, you can tune for
## the optimal value of number of EPOCHS where the linear percetpreon gives the
## highest overall accuracy.
##
## Written in R
##
## Pooya Taherkhani
## pt376511 AT ohio DOT edu
## December 2016
## =============================================================================


rm(list=ls())

EPOCHS_MAX <- 20                        # EPOCHS set to 1, ..., EPOCHS_MAX

EXPERIMENT <- 1                         # 0/1: change this to 0 ONLY if you want
                                        # to update the graphs in the report

KERNEL_TYPE <- "lin"                   # lin: linear

## POLYNOMIAL PARAMETERS:
c <- 1                                  # c: constant
d <- 2                                  # d: degree of polynomial

TOL    <- 0.01                           # PERCENTAGE of error tolerance


## =============================================================================
## READ data from a CSV file and LOAD it to a list.
##
## TAKE IN filename: a string that contains the name of a CSV file to be read
##
## RETURN  a list of:
##         PHI     : PHI matrix, the design matrix that has feature vectors
##                   (including bias feature) of each data point (without the
##                   labe) at each row
##         t_vector: vector of labels
##         N_samples:  number of samples
##         N_features: number of features (including the bias feature, which is
##                                         always 1)
## =============================================================================
load_data <- function( filename)
{
  dataset     <- as.matrix( read.csv( filename, header = FALSE))

  N_samples  <- dim( dataset)[ 1]
  N_features <- dim( dataset)[ 2]    # -1+1: feature vector doesn't include
                                        # the label, but it does include the bias
                                        # feature, which is always 1
  ## separate labels from data, then add 1 's column
  features_mat <- dataset[, seq_len( N_features-1)]
  ones_vec <- rep_len( 1, N_samples)
  PHI      <- cbind( ones_vec, features_mat)
  t_vector <- dataset[, N_features]

  raw_data <- list( PHI       = PHI,
                   t_vector   = t_vector,
                   N_samples  = N_samples,
                   N_features = N_features)
}


## =============================================================================
## EXTRACT mininum and maximum of each feature from the TRAINING dataset
##
## TAKE IN filename that contains TRAINING data -- call it only on
##         "../data/digits.tra"
##
## RETURN a list of:
##        min_vec: a vector of minimum feature values across samples
##        max_vec: a vector of maximum feature values across samples
##
## CALL load_data
## =============================================================================
min_max_features <- function( filename)
{
  raw_data <- load_data( filename)
  
  PHI <- raw_data $ PHI
  
  min_vec <- apply( PHI, 2, min)
  max_vec <- apply( PHI, 2, max)

  min_max <- list( min_vec = min_vec,
                  max_vec = max_vec)
}


## =============================================================================
## SCALE features based on the minimum and maximum value of each feature in the
## TRAINING dataset
##
## TAKE IN filename: of the file that contains the data to be scaled
##
## RETURN a list of:
##         PHI     : SCALED PHI matrix
##         t_vector: vector of labels
##         N_samples:  number of samples
##         N_features: number of features
##
## CALL load_data, min_max_features
## =============================================================================
scale_data <- function( filename)
{
  raw_data <- load_data( filename)

  PHI        <- raw_data $ PHI
  t_vector   <- raw_data $ t_vector
  N_samples  <- raw_data $ N_samples
  N_features <- raw_data $ N_features      
  
  min_max <- min_max_features( "../data/digits.tra")
  min_vec <- min_max $ min_vec
  max_vec <- min_max $ max_vec

  ## find the features in the TRAINING data that have the same value across
  ## all examples (by the way, the model doesn't learn anything from these,
  ## so I call them empty_feature!) and replace them with original values
  feature_range <- max_vec - min_vec          # a vector
  zero_idx      <- which( feature_range == 0) # index of constant features

  ## refill the vectors to matrices row-wise to compute scaled_PHI
  min_mat <- matrix( min_vec, N_samples, N_features, byrow = TRUE)
  max_mat <- matrix( max_vec, N_samples, N_features, byrow = TRUE)

  scaled_PHI <- (PHI - min_mat) / (max_mat - min_mat)
  scaled_PHI[, zero_idx] <- PHI[, zero_idx] # restore constant features

  ## find NaN's in the scaled_PHI and replace them with orginial feature values
  nan_idx <- which( is.nan( scaled_PHI), arr.ind = TRUE)
  scaled_PHI[ nan_idx] <- PHI[ nan_idx]

  ## find Inf's in the scaled_PHI and replace them with orginial feature values
  inf_idx <- which( scaled_PHI == Inf, arr.ind = TRUE)
  scaled_PHI[ inf_idx] <- PHI[ inf_idx]

  ## Also for -Inf
  m_inf_idx <- which( scaled_PHI == -Inf, arr.ind = TRUE)
  scaled_PHI[ m_inf_idx] <- PHI[ m_inf_idx]

  ## find values smaller than minimum and replace them with minimum
  too_low_ind <- which( scaled_PHI < min_mat, arr.ind = TRUE)
  scaled_PHI[ too_low_ind] <- min_mat[ too_low_ind]

  ## find values greater than maximum and replace them with maximum
  too_high_ind <- which( scaled_PHI < min_mat, arr.ind = TRUE)
  scaled_PHI[ too_high_ind] <- min_mat[ too_high_ind]

  scaled_data <- list( PHI       = scaled_PHI,
                      t_vector   = t_vector,
                      N_samples  = N_samples,
                      N_features = N_features)
}


## =============================================================================
## COMPUTES the sgn (modified sign) function that can take in a VECTOR.
## sgn( 0) = 1, this is where it differs from a normal sign function
## 
## TAKE IN a vector (of numbers!)
##
## RETURN a vector of sgn of the input numbers
## =============================================================================
modified_sign <- function( x)
{
  s <- sign( x)
  s[ s == 0] <- 1                     # replace all 0 with 1
  return( s)
}


## =============================================================================
## TRAIN LINEAR PERCEPTRON
##
## TAKE IN train_data: scaled training data as a MATRIX that contains design
##                     matrix PHI and the vector of labels
##         EPOCHS:     maximum number of epochs allowed
##
## RETURN w_vector: the learned weight vector
##        epoch:    the epoch number at which linear perceptron stops/converges
##
## CALL modified_sign, norm_vec
## =============================================================================
train_perceptron <- function(train_data, EPOCHS)
{
  ## training data -- design matrix and vector of labels
  PHI        <- train_data$PHI
  t_vector   <- train_data$t_vector
  N_samples  <- train_data$N_samples
  N_features <- train_data$N_features

  w_vector <- numeric(N_features)     # w initialzed to a vector of zeros
  epoch    <- 1                       # epoch iterator variable

  repeat
  {
    w_prev <- w_vector
    for (i in seq_len(N_samples)) { # iterate over training samples
      phi_vector <- PHI[i, ]      # feature vector for a single sample
      t   <- t_vector[i]          # a single label
      y   <- modified_sign( w_vector %*% phi_vector )
      if ( y != t)
        w_vector <- w_vector + t * phi_vector 
    }

    w_cur <- w_vector

    ## The convergence condition is based on absolute relative error
    converged <- (norm_vec(w_cur - w_prev) <= TOL * norm_vec(w_prev))

    ## terminate the loop if w has converged or EPOCHS limit is reached
    if (converged || epoch == EPOCHS)
      break

    epoch <- epoch + 1
  }

  trained_param <- list(w     = w_vector,
                        epoch = epoch)
}


## =============================================================================
## compute TRAINING ACCURACY after a model is trained using PERCEPTRON
## this function is not used in the current program
## =============================================================================
training_accuracy <- function(train_data, w_vector)
{
  PHI        <- train_data$PHI
  t_vector   <- train_data$t_vector
  N_samples  <- train_data$N_samples
  ##N_features <- train_data$N_features

  y_vector <- modified_sign( PHI %*% w_vector)
  ## print( cbind( y_vector, t_vector))

  yt             <- y_vector * t_vector
  correct_vector <- yt[ yt == 1]
  correct_count  <- length( correct_vector)

  correct_count / N_samples * 100     # accuracy (performance) percent
}


## =============================================================================
## compute norm of a vector (L-2 norm: magnitude of a vector)
## =============================================================================
norm_vec <- function( x)
{
  sqrt( sum( x^2))
}

## =============================================================================
## This function will be used in prediction_accuracy function to TUNE for the
## best value of EPOCHS (T).
## 
## TRAIN the linear perceptron for a RANGE of number of epochs, 1 to EPOCHS_MAX.
## For each particular value of EPOCHS, train the perceptron for all digits.
## Then NORMALIZE the weight vectors for ALL digits and for ALL epoch values.
##
## TAKE IN  nothing
##
## RETURN  w_3d_array: a 3-dimensional array of normalized weights
##                    w_3d_array[ k-th weight, digit+1, EPOCHS]
##                    the learned weight vector for a particular digit at a
##                    particular number of EPOCHS is
##                    w_3d_array[, digit+1, EPOCHS]
##
## CALL  scale_data, train_percpetron, & norm_vec
## =============================================================================
train_and_normalize <- function()
{
  w_3d_array <- numeric()             # store normalized weight vectors for
                                        # all digits for all EPOCHS w_3d_array[
                                        # w_matrix, EPOCHS]
  for (EPOCHS in seq_len( EPOCHS_MAX)) {
    cat('EPOCHS =', EPOCHS, '\n')

    ## for each digit, scale the data
    ## and train the linear percptron
    w_matrix <- numeric()           # store normalized weight vectors for
                                        # all digits for a particular value of
                                        # EPOCHS
    for (digit in seq.int(0, 9)) {
      filename    <- paste0("../data/digits-", digit, ".tra")
      train_data    <- scale_data( filename)
      trained_param <- train_perceptron( train_data, EPOCHS)
      w             <- trained_param$w
      normalized_w  <- w / norm_vec( w)
      w_matrix      <- cbind( w_matrix, normalized_w)
    }
    w_3d_array    <- array( c( w_3d_array, w_matrix),
                           dim=c( dim( w_matrix), EPOCHS))
  }
  return(w_3d_array)
}

## =============================================================================
## This function is the second function that helps to select for epoch numbers
## (T) the value that obtains the best overall accuracy on an unseen set of data.
##
## After SCALING the unseen dataset, PREDICT the digits correspoinding to the
## dataset based on the learned weights. Then compute the ACCURACY based on the
## prediction.
##
## TAKE IN the unseen data filename and the normalized 3 dimensional weight
## array that contains the learned weight vectors for all 10 digits for all
## EPOCHS
##
## RETURN the prediction accuracy of the trained model on the unseen dataset
##
## CALL scale_data,  
## =============================================================================
prediction_accuracy <- function( filename, w_3d_array)
{
  dev_data <- scale_data(filename)
  
  PHI        <- dev_data $ PHI
  t_vector   <- dev_data $ t_vector
  N_samples  <- dev_data $ N_samples
  N_features <- dev_data $ N_features

  if (N_features != dim( w_3d_array)[1]) {
    stop('The length of the weight vector does NOT match the length of feature vectors!\nComputation terminated!')
  }

  ## compute accuracy for each value of EPOCHS
  accuracy <- numeric()               # a vector to store accuracy for EPOCHS
  for (EPOCHS in seq.int( EPOCHS_MAX)) {
    scores_matrix <- PHI %*% w_3d_array[,,EPOCHS]

    ## print( 'scores_matrix dimension')
    ## print( dim( scores_matrix))

    y_vector      <- apply( scores_matrix, 1, which.max) - 1
    correct_count <- length( y_vector[ y_vector == t_vector])

    accuracy <- c( accuracy, correct_count / N_samples * 100)

    ## print( 'y_vector')
    ## print( y_vector)
  }
  return( accuracy)
}


cat('============== NEW RUN ==============\n')
##cat('Tolerance:', TOL, '\n')

norm_w_3d_array <- train_and_normalize()
accuracy <- prediction_accuracy( "../data/digits.dev", norm_w_3d_array)
epochs <- 1:EPOCHS_MAX
accuracy_df <- data.frame( epochs, accuracy)
cat('======== accuracy vs. epochs ========\n')
accuracy_df

## PLOT prediction ACCURACY for development data versus number of EPOCHS
if ( require( "ggplot2")) {
  ## print(
    ggplot( accuracy_df, aes( epochs, y=accuracy)) +
    geom_line( aes( y=accuracy), color="blue") +
    geom_point( aes( y=accuracy), fill="white", color="blue", shape = 21) +
    ## xlim( 1, 20) +
    ## ylim( 91, 96) +
    xlab( "Epochs, T") +
    ylab( "Overall Accuracy (%)") +
    scale_x_continuous( breaks = seq( 0,20,2))
  ## )
} else {
  plot( accuracy, type = 'o', xlim = c(1,EPOCHS_MAX), xlab = 'Epochs', ylab = 'Overall Accuracy (%)')
}

## SAVE GRAPH
if (!EXPERIMENT) {                      # update graphs in the report
  ggsave("../report/graphs/percep-accuracy-dev.pdf", width=5, height=3.4)
} else {                                # update experimental grphs only!
  ggsave("percep-accuracy-dev.pdf", width=5, height=3.4)
  cat("Find the plot at percep-accuracy-dev.pdf\n")
}

## ====== SAVE RESULTS ======
## save.image( "linear-percep.RData")
