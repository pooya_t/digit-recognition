#!/bin/bash
for gamma in 50 2 0.125 0.02 0.005
do
    ./svm-train -t 2 -g $gamma -q svm-digits.tra.scaled gauss-model
     printf "gamma = $gamma:  "; ./svm-predict svm-digits.dev.scaled gauss-model output-gauss
done
