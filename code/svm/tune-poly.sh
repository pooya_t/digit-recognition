#!/bin/bash
for deg in 2 3 4 5 6
do
    ./svm-train -t 1 -d $deg -g 1 -r 1 -q svm-digits.tra.scaled poly-model
    printf "degree = $deg:  "; ./svm-predict svm-digits.dev.scaled poly-model output-poly
done
